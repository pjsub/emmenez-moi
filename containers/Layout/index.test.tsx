import React from "react";
import { shallow } from "enzyme";
import LayoutIndex from "./index";

describe("<LayoutIndex />", () => {
  it("should render without crashing", () => {
    const wrapper = shallow(
      <LayoutIndex>
        <p>Hello Loft Orbital</p>
      </LayoutIndex>
    );
    expect(wrapper.find("p").text()).toBe("Hello Loft Orbital");
  });
});
