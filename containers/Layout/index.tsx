import React from "react";
import Theme from "../../styles";
import HeadCustom from "../../components/HeadCustom";

type Props = {
  children: React.ReactNode;
};

// This is the default layout of the app
function LayoutIndex({ children }: Props) {
  return (
    <Theme>
      <HeadCustom />
      {children}
    </Theme>
  );
}

export default LayoutIndex;
