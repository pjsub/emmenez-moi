# Work Summary

### 1 - Design prototype

The first part is design prototying. How the app is going to look like and how components are going to be built. Drawing on a paper some sketch is the very first part, then making a fancy design on Figma / Illustrator is the second part.

### 2 - Project setup

Setup of the NextJS project is the second part. Making sure everything is installed.

### 3 - Development : Containers & Components part

Creating containers and components is the first coding part. Components should be reusable. At the same time tests are also written and launch to be sure every single components works independently from the whole app.

### 4 - Pages design

Pages are then coded: the index page and the country page. They must be an essembly of conainters and components.

### 5 - Logic, GraphQL and other side effects

Once the design is ready, query fetching features can be implemented. This part of the code implemented with caution because of the many side effects that can occur client side and server side.

### 6 - Graphical modification & UX

When everything works as intented, it's time make things fancier! UX is also checked and modified if needed. Illustrator comes handy.

### 7 - Final test

This is the last step before deploying the app. Everythink should be ready, logs are inspected.

### 7 - Deployment setup

Setting up docker file in order to make deployment easier on every environment.
