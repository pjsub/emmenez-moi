import React, { useState, useEffect } from "react";
import LayoutIndex from "../../containers/Layout";
import FullScreen from "../../components/FullScreen";
import Logo from "../../components/Logo";
import Footer from "../../components/Footer";
import CountryTitle from "../../components/CountryTitle";
import CountryContinent from "../../components/CountryContinent";
import CountryData from "../../components/CountryData";
import { useQuery } from "@apollo/react-hooks";
import { GET_COUNTRY } from "../../utils/ApolloQueries";
import formatNumber from "../../utils/FormatNumbers";
import Loading from "../../components/Loading";
import CountryCities from "../../components/CountryCities";
import ReactMapGL, { Marker } from "react-map-gl";

import { useRouter } from "next/router";

const Country = () => {
  const router = useRouter();
  const { loading, error, data } = useQuery(GET_COUNTRY, {
    variables: { id: router.query.id },
    skip: router === undefined,
  });

  const [viewport, setViewport] = useState({
    width: 250,
    height: 250,
    latitude: 0,
    longitude: 0,
    zoom: 8,
  });

  useEffect(() => {
    setViewport({
      width: 250,
      height: 250,
      latitude:
        data?.countries[0].capital === null
          ? 0
          : data?.countries[0].capital?.location.lat,
      longitude:
        data?.countries[0].capital === null
          ? 0
          : data?.countries[0].capital?.location.long,
      zoom: 2,
    });
  }, [data]);

  // Yup, this is really bad
  // As it may be deployed by reviewer on their own environement, token can't be hidden in env file
  const TOKEN =
    "pk.eyJ1IjoicGpzdWIiLCJhIjoiY2s4cHFvOHlsMGZ5bjNmcHMybHFqcHAwbCJ9.TXBfVF2cIsnk2zpbKyBcHw";

  if (loading) return <Loading />;

  return (
    <LayoutIndex>
      <FullScreen image={data?.countries[0].id}>
        <div>
          <ReactMapGL
            {...viewport}
            mapboxApiAccessToken={TOKEN}
            onViewportChange={setViewport}
            style={{ float: "right", margin: "15px" }}
          >
            <Marker
              latitude={data?.countries[0].capital?.location.lat ?? 0}
              longitude={data?.countries[0].capital?.location.long ?? 0}
              offsetLeft={-10}
              offsetTop={-20}
            >
              <div style={{ width: "25px" }}>
                <img src="/svg/map-red.svg" />
              </div>
            </Marker>
          </ReactMapGL>
        </div>
        <Logo />
        <CountryContinent>{data?.countries[0].continent.name}</CountryContinent>
        <CountryTitle>{data?.countries[0].name}</CountryTitle>
        <CountryData info="Capital" icon="map">
          {data?.countries[0].capital?.name ?? "?"}
        </CountryData>
        <CountryData info="Population" icon="inhabitant">
          {formatNumber(data?.countries[0].population)}
        </CountryData>
        <CountryData info="Languages" icon="language">
          {data?.countries[0].languages
            .map((elem: any) => elem.name)
            .join(", ")}
        </CountryData>
        <CountryData info="VAT Rate" icon="percentage">
          {data?.countries[0].vatRate}
        </CountryData>
        <CountryData info="Calling Codes" icon="phone">
          {data?.countries[0].callingCodes}
        </CountryData>
        <CountryCities cities={data?.countries[0].cities} />
        <Footer />
      </FullScreen>
    </LayoutIndex>
  );
};

export default Country;
