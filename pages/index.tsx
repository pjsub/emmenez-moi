import React from "react";
import Layout from "../containers/Layout";
import FullScreen from "../components/FullScreen";
import Logo from "../components/Logo";
import SearchBar from "../components/SearchBar";
import Footer from "../components/Footer";
import { useQuery } from "@apollo/react-hooks";
import Loading from "../components/Loading";

import { GET_COUNTRIES } from "../utils/ApolloQueries";

// Home page of the app
function Home() {
  const { loading, error, data } = useQuery(GET_COUNTRIES);

  if (loading) return <Loading />;

  return (
    <Layout>
      <FullScreen image="default">
        <Logo />
        <SearchBar countriesList={data.countries} />
        <Footer />
      </FullScreen>
    </Layout>
  );
}

export default Home;
