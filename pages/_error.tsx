import React, { useState, useEffect } from "react";
import Layout from "../containers/Layout";
import FullScreen from "../components/FullScreen";
import Logo from "../components/Logo";
import Footer from "../components/Footer";
import ErrorCustom from "../components/ErrorCustom";

type Props = {
  statusCode: number;
  res: any;
  err: any;
};

function Error({ statusCode }: Props) {
  const [isLoaded, setIsLoaded] = useState<boolean>(false);

  // Make render waits until everything is loaded
  useEffect(() => {
    setIsLoaded(true);
  }, []);

  if (!isLoaded) return null;
  return (
    <Layout>
      <FullScreen image="error">
        <Logo />
        <ErrorCustom code={statusCode} info="Oops, we got too far" />
        <Footer />
      </FullScreen>
    </Layout>
  );
}

Error.getInitialProps = ({ res, err }: Props) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
  return { statusCode };
};

export default Error;
