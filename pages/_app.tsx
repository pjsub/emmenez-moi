import React from "react";
import App from "next/app";
import { AppProps } from "next/app";
import { ApolloProvider } from "@apollo/react-hooks";

import withData from "../utils/ApolloClient";

// We wrap the whole app to the Apollo provider
function MyApp({ Component, pageProps, apollo }: AppProps | any) {
  return (
    <ApolloProvider client={apollo}>
      <Component {...pageProps} />
    </ApolloProvider>
  );
}

export default withData(MyApp);
