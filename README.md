# EMMENEZ-MOI

EMMENEZ-MOI is a light travel app aim to make the user eager to travel.
It has been done in 1 day for the technical test of Loft Orbital.

Live Demo: https://emmenez-moi.algernhon.now.sh

# Technologies

Solutions and technologies used by this app.

### Typescript

TypeScript is a typed superset of JavaScript that compiles to plain JavaScript. This choise has be driven to avoid errors and to make code clearer for readers.

https://www.typescriptlang.org/

### NextJS

NextJS is a React framework aimed to make server rendering app easier. This choise has be driven by the "zero configuration" feature and because it is also SEO friendly.

https://nextjs.org/

### Apollo & Apollo Boost

GraphQL queries were needed in this test. To achieve these latters, Apollo is used thanks to its React client.

https://www.apollographql.com/

### Styled Components

Styled components is an css in js library. It make components reusable and it is easier to deal with JS condition without creating countless className.

https://styled-components.com/

### react-map-gl

React Map QL is a MapBox React implementation. This is one of the few Map framworks that correctly works with Typescript.

https://uber.github.io/react-map-gl/

# Installation

For development environments

```sh
$ git clone https://gitlab.com/pjsub/emmenez-moi.git
$ cd emmenez-moi
$ npm run dev
```

For production environments.

```sh
$ npm run build
$ npm run start
```

For Docker environments.

```sh
docker-compose up --build
```

For tests

```sh
$ npm run test
```

# Todos

- More tests
- Responsive design
- Animations
- Better cities presentations
- Cleaner graphql calls
- Cleaner containers

## License

MIT
