import { createGlobalStyle } from "styled-components";

type ThemeType = {
  colors: {
    defaultBG: string;
    text: string;
  };
};

// This is global theme, this shouldn't be modified unless it's strictly necessary /!\
const GlobalTheme = createGlobalStyle<{ theme: ThemeType }>`
  *,
  *::after,
  *::before {
    box-sizing: border-box;
  }

  body {
    background: ${(props) => props.theme.colors.defaultBG};
    color: ${(props) => props.theme.colors.text};
    font-family: 'Open Sans', sans-serif;
    font-weight: 400;
    height: 100%;
    margin: 0;
    padding: 0;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
`;

export default GlobalTheme;
