import React from "react";
import styled from "styled-components";

const Title = styled.div`
  color: white;
  font-size: 40px;
  font-weight: 700;
  text-transform: uppercase;
  margin-left: 2rem;
  margin-bottom: -0.5rem;
`;

const Icon = styled.img`
  margin-right: 20px;
  max-width: 26px;
  max-height: 32px;
`;

type Props = {
  children: React.ReactNode;
  icon: string;
  info: string;
};

function Population({ children, icon, info }: Props) {
  return (
    <>
      <Title title={info}>
        <Icon src={`/svg/${icon}.svg`} />
        {children ?? "?"}
      </Title>
    </>
  );
}

export default Population;
