import React from "react";
import { shallow } from "enzyme";
import CountryData from "./index";

describe("<CountryData />", () => {
  it("should render without crashing", () => {
    const wrapper = shallow(
      <CountryData icon="icontest" info="infotest">
        <p>Hello Loft Orbital</p>
      </CountryData>
    );
    expect(wrapper.find("p").text()).toBe("Hello Loft Orbital");
  });
});
