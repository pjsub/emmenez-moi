import React from "react";
import styled from "styled-components";
import ReactLoading from "react-loading";
import Layout from "../../containers/Layout";

const Logo = styled.img`
  margin-bottom: 2em;
  width: 18em;
`;

const Center = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  height: 100vh;
  width: 100vw;
`;

function Loading() {
  return (
    <Layout>
      <Center>
        <div>
          <Logo src="/svg/logo.svg" alt="Emmenez-Moi Logo" />
        </div>
        <div>
          <ReactLoading
            type="spin"
            color="black"
            height={"70px"}
            width={"70px"}
          />
        </div>
      </Center>
    </Layout>
  );
}

export default Loading;
