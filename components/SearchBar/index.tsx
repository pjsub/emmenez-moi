import React, { useState } from "react";
import styled from "styled-components";
import Router from "next/router";

const SearchDiv = styled.div`
  margin-top: 5em;
  width: 100%;
`;

const Form = styled.form`
  background-color: white;
  border-radius: 15px;
  width: 400px;
  display: flex;
  flex-direction: row;
  margin: auto;
`;

const Input = styled.input`
  background-color: transparent;
  border: none;
  color: ${(props) => props.theme.colors.text};
  flex-grow: 2;
  font-size: 24px;
  padding: 0 0.9em;
  height: 2em;

  &:focus {
    outline: 0;
  }

  & ::placeholder {
    opacity: 0.4;
  }
`;

const Button = styled.button`
  background: url("svg/search-solid.svg");
  background-position: center;
  background-repeat: no-repeat;
  background-size: contain;
  border: none;
  width: 25px;
  margin-right: 1em;

  & :hover {
    cursor: pointer;
    opacity: 0.7;
  }

  & :focus {
    outline: 0;
  }
`;

const Label = styled.div`
  color: white;
  font-size: 34px;
  text-align: center;
  font-weight: 700;
  text-transform: uppercase;
`;

const SubLabel = styled.div`
  color: white;
  font-size: 34px;
  margin-bottom: 1em;
  text-align: center;
  text-transform: uppercase;
`;

const List = styled.ul`
  background-color: white;
  border-radius: 15px;
  margin: auto;
  margin-top: 1em;
  padding: 0;
  width: 400px;
`;

const Items = styled.li`
  border-radius: 15px;
  color: ${(props) => props.theme.colors.text};
  padding: 0.8em;
  list-style-type: none;

  & :hover {
    background-color: #eceff4;
    cursor: pointer;
  }
`;

const ItemsActive = styled.li`
  border-radius: 15px;
  color: ${(props) => props.theme.colors.text};
  padding: 0.8em;
  list-style-type: none;
  background-color: #eceff4;
  & :hover {
    cursor: pointer;
  }
`;

type Country = {
  name: string;
  id: string;
  __typename: string;
};

type Props = {
  countriesList: Country[];
};

function SearchBar({ countriesList }: Props) {
  const [filteredCountries, setFilteredCountries] = useState<Country[]>([]);
  const [searchInput, setSearchInput] = useState<string>("");
  const [searchActive, setSearchActive] = useState<number>(0);
  const [searchId, setSearchId] = useState<string>("");

  function onChange(event: React.ChangeEvent<HTMLInputElement>) {
    const countries = countriesList;
    const userInput = event.currentTarget.value;

    const filteredCountries = countries.filter(
      (countries) =>
        countries.name.toLowerCase().indexOf(userInput.toLowerCase()) > -1
    );

    setFilteredCountries(filteredCountries);
    setSearchInput(userInput);
    setSearchActive(0);
  }

  function onClick(event: React.MouseEvent<HTMLLIElement>) {
    setFilteredCountries([]);
    setSearchInput(event.currentTarget.innerText);
    setSearchId(filteredCountries[searchActive].id);
    setSearchActive(0);
  }

  function onKeyDown(event: React.KeyboardEvent<HTMLInputElement>) {
    // Key 13 is enter/return
    // Key 38 is up arrow
    // Key 40 is down arrow
    if (event.keyCode === 13) {
      setSearchInput(filteredCountries[searchActive].name);
      setSearchId(filteredCountries[searchActive].id);
      setSearchActive(0);
    } else if (event.keyCode === 38) {
      if (searchActive === 0) return;
      setSearchActive(searchActive - 1);
    } else if (event.keyCode === 40) {
      if (searchActive + 1 === filteredCountries.length) return;
      setSearchActive(searchActive + 1);
    }
  }

  function OptionList() {
    let listItems = null;

    // Workaround: TODO scroll list if enough time
    if (filteredCountries.length <= 6) {
      listItems = filteredCountries.map((optionName, index) => {
        if (index === searchActive) {
          return (
            <ItemsActive key={optionName.id} onClick={onClick}>
              {optionName.name}
            </ItemsActive>
          );
        } else {
          return (
            <Items key={optionName.id} onClick={onClick}>
              {optionName.name}
            </Items>
          );
        }
      });
    }

    return <List>{listItems}</List>;
  }

  function handleOnSubmit(event: React.FormEvent) {
    event.preventDefault();
    Router.push("/" + searchInput.replace(/\s/gi, "-") + "/" + searchId);
  }

  return (
    <SearchDiv>
      <Label>Choose a country,</Label>
      <SubLabel>we'll handle the rest!</SubLabel>
      <Form onSubmit={handleOnSubmit}>
        <Input
          type="text"
          placeholder="France"
          onChange={onChange}
          onKeyDown={onKeyDown}
          value={searchInput}
          required
        />
        <Button />
      </Form>
      <OptionList />
    </SearchDiv>
  );
}

export default SearchBar;
