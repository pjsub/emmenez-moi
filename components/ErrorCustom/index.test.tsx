import React from "react";
import { shallow } from "enzyme";
import ErrorCustom from "./index";

describe("<ErrorCustom />", () => {
  it("should render without crashing", () => {
    const wrapper = shallow(<ErrorCustom code={404} info="error" />);
    expect(wrapper.find("ErrorInfo").children().text()).toBe("error");
  });
});
