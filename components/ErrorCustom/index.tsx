import React from "react";
import styled from "styled-components";

const ErrorWrapper = styled.div`
  margin-top: 5em;
  width: 100%;
`;

const ErrorCode = styled.div`
  color: white;
  font-size: 34px;
  text-align: center;
  font-weight: 700;
  text-transform: uppercase;
`;

const ErrorInfo = styled.div`
  color: white;
  font-size: 34px;
  margin-bottom: 1em;
  text-align: center;
  text-transform: uppercase;
`;
ErrorInfo.displayName = "ErrorInfo"; // Testing puprose only

type Props = {
  info: string;
  code?: number;
};

function ErrorCustom({ code, info }: Props) {
  return (
    <ErrorWrapper>
      {code ? <ErrorCode>{code}</ErrorCode> : <ErrorCode>Oops</ErrorCode>}
      <ErrorInfo>{info}</ErrorInfo>
    </ErrorWrapper>
  );
}

export default ErrorCustom;
