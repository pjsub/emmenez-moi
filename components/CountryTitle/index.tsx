import React from "react";
import styled from "styled-components";

const Title = styled.div`
  color: white;
  font-size: 64px;
  font-weight: 700;
  text-transform: uppercase;
  margin-left: 2rem;
`;

type Props = {
  children: React.ReactNode;
};

function CountryTitle({ children }: Props) {
  return <Title>{children}</Title>;
}

export default CountryTitle;
