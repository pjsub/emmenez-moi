import React from "react";
import { shallow } from "enzyme";
import CountryTitle from "./index";

describe("<CountryTitle />", () => {
  it("should render without crashing", () => {
    const wrapper = shallow(
      <CountryTitle>
        <p>Hello Loft Orbital</p>
      </CountryTitle>
    );
    expect(wrapper.find("p").text()).toBe("Hello Loft Orbital");
  });
});
