import React from "react";
import styled from "styled-components";

const Continent = styled.div`
  color: white;
  font-size: 24px;
  text-transform: uppercase;
  margin: 6rem 0 -1rem 2.2rem;
`;

type Props = {
  children: React.ReactNode;
};

function CountryTitle({ children }: Props) {
  return <Continent>{children}</Continent>;
}

export default CountryTitle;
