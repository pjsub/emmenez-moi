import React from "react";
import { shallow } from "enzyme";
import ContryContinent from "./index";

describe("<ContryContinent />", () => {
  it("should render without crashing", () => {
    const wrapper = shallow(
      <ContryContinent>
        <p>Hello Loft Orbital</p>
      </ContryContinent>
    );
    expect(wrapper.find("p").text()).toBe("Hello Loft Orbital");
  });
});
