import React from "react";
import Link from "next/link";
import styled from "styled-components";

const LogoDiv = styled.div`
  padding: 2em;
`;

const LogoImg = styled.img`
  cursor: pointer;
  width: 13em;
`;

function Logo() {
  return (
    <LogoDiv>
      <Link href="/">
        <LogoImg src="/svg/logo-white.svg" alt="Emmenez-Moi Logo" />
      </Link>
    </LogoDiv>
  );
}

export default Logo;
