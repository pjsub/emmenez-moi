import React from "react";
import styled from "styled-components";

const FooterDiv = styled.div`
  bottom: 0px;
  font-size: 12px;
  color: white;
  position: absolute;
  text-align: center;
  width: 100%;
  margin-bottom: 1em;
  opacity: 0.6;
  filter: grayscale(100%);
`;

// Footer with an easter egg!
function Footer() {
  return (
    <FooterDiv>
      2020 - By Pierre-Jean{" "}
      <a
        href="https://www.youtube.com/watch?v=0OrKMaeQUx0"
        target="_blank"
        rel="nofollow noopener noreferrer"
      >
        🌍
      </a>
    </FooterDiv>
  );
}

export default Footer;
