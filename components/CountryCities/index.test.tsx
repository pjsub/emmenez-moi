import React from "react";
import { shallow } from "enzyme";
import CountryCities from "./index";

describe("<LayoutIndex />", () => {
  it("should render without crashing", () => {
    const city = [
      {
        name: "test",
        location: {
          lat: 0,
          long: 0,
        },
      },
    ];
    const wrapper = shallow(<CountryCities cities={city} />);
    expect(wrapper.find("a").text()).toBe("test");
  });
});
