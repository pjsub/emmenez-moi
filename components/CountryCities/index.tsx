import React from "react";
import styled from "styled-components";
import shuffle from "../../utils/Shuffle";

const Wrapper = styled.div`
  bottom: 4rem;
  position: absolute;
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  width: 100%;
  & a {
    text-decoration: none;
  }
`;

const City = styled.div`
  border: 1px solid white;
  border-radius: 15px;
  color: white;
  font-weight: 700;
  padding: 10px 15px;
  opacity: 1;
  position: relative;
  transition: all 1s ease;

  & :hover {
    cursor: pointer;
    background-color: white;
    color: ${(props) => props.theme.colors.text};
    padding: 10px 30px;
    opacity: 1;
  }
`;

type Loc = {
  lat: number;
  long: number;
};

type CitiesT = {
  name: string;
  location: Loc;
};

type Props = {
  cities: CitiesT[];
};

function CountryCities({ cities }: Props) {
  // Let's show 5 random cities from this country
  const shuffleCity = shuffle(cities).slice(0, 5);

  return (
    <Wrapper>
      {shuffleCity.map((city: CitiesT) => (
        <a
          href={`https://www.google.fr/maps/@${city.location.lat},${city.location.long},13z`}
          target="_blank"
          rel="nofollow noopener noreferrer"
          key={city.name}
        >
          <City>{city.name}</City>
        </a>
      ))}
    </Wrapper>
  );
}

// React memo used in order to not shuffle again when re-render occurs
export default React.memo(CountryCities);
