import React from "react";
import Head from "next/head";

// This is the custom HTML <head> of the app
function HeadCustom() {
  return (
    <Head>
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1, shrink-to-fit=no"
        key="viewport"
      />
      <meta
        name="description"
        content="Discover new beautiful countries all over the world"
      />
      <title>EMMENEZ-MOI - A new way to plan your travel</title>
      <link
        rel="icon"
        href="/img/favicon-16x16.png"
        sizes="16x16"
        type="image/png"
        key="favicon"
      />
      <link
        href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap"
        rel="stylesheet"
      />
    </Head>
  );
}

export default HeadCustom;
