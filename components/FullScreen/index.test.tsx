import React from "react";
import { shallow } from "enzyme";
import FullScreen from "./index";

describe("<FullScreen />", () => {
  it("should render without crashing", () => {
    const wrapper = shallow(
      <FullScreen image="test">
        <p>Hello Loft Orbital</p>
      </FullScreen>
    );
    expect(wrapper.find("p").text()).toBe("Hello Loft Orbital");
  });
});
