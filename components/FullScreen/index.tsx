import React from "react";
import styled from "styled-components";

type Props = {
  children: React.ReactNode;
  image: string;
};

type PropsStyle = {
  background: string;
};

const FullDiv = styled.div`
  height: 100vh;
  width: 100vw;
  padding: 7vh 5vw;
  position: relative;
  background-color: ${(props) => props.theme.colors.defaultBG};
`;

const ImageDiv = styled.div<PropsStyle>`
  background-image: url("/img/${(props) => props.background}.jpg");
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  height: 100%;
  position: relative;
  -webkit-box-shadow: 0px 6px 10px 0px rgba(50, 50, 50, 0.3);
  -moz-box-shadow: 0px 6px 10px 0px rgba(50, 50, 50, 0.3);
  box-shadow: 0px 6px 10px 0px rgba(50, 50, 50, 0.3);
`;

function FullScreen({ children, image }: Props) {
  return (
    <FullDiv>
      <ImageDiv background={image}>{children}</ImageDiv>
    </FullDiv>
  );
}

export default FullScreen;
