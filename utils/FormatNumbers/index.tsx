export default function formatNumber(num: number) {
  if (num === undefined) return "0";
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}
