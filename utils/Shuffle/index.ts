export type Loc = {
  lat: number;
  long: number;
};

export type CitiesT = {
  name: string;
  location: Loc;
};

export default function shuffle(a: CitiesT[]) {
  for (let i = a.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [a[i], a[j]] = [a[j], a[i]];
  }
  return a;
}
