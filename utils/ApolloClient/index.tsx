import { ApolloClient } from "apollo-client";
import { InMemoryCache } from "apollo-cache-inmemory";
import withApollo from "next-with-apollo";
import { createHttpLink } from "apollo-link-http";
import fetch from "isomorphic-unfetch";

const GRAPHQL_URL = "https://api.everbase.co/graphql?apikey=alpha";

const link = createHttpLink({
  fetch,
  uri: GRAPHQL_URL,
});

const client = new ApolloClient({
  link: link,
  cache: new InMemoryCache(),
});

export default withApollo(
  ({ initialState }) =>
    new ApolloClient({
      link: link,
      cache: new InMemoryCache().restore(initialState || {}),
    })
);
