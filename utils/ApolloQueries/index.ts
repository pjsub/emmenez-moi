import gql from "graphql-tag";

export const GET_COUNTRIES = gql`
  {
    countries {
      name
      id
    }
  }
`;

export const GET_COUNTRY = gql`
  {
    countries(where: { id: { eq: $id } }) {
      id
      name
      cities {
        name
        location {
          lat
          long
        }
      }
      continent {
        name
      }
      languages {
        name
      }
      population
      vatRate
      callingCodes
      capital {
        name
        population
        timeZone {
          name
          offset
        }
        location {
          lat
          long
        }
      }
    }
  }
`;
